#!/usr/bin/env perl


use Data::Dumper;
use Geo::libpostal ':all';

my %location_obj = parse_address($ARGV[0]);
print Dumper(\%location_obj);

exit 0;
