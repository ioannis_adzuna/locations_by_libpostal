#!/usr/bin/env perl

use strict;
use warnings;
use utf8;

use Data::Dumper;
use Encode qw(decode);
use I18N::Langinfo qw(langinfo CODESET);

# set binmode
my $codeset = langinfo(CODESET);
binmode STDOUT, ":encoding($codeset)";


while ( my $line = readline(STDIN) ) {

    $line = decode($codeset, $line);

    # remove country
    $line =~ s/ru|russia|Россия//gsiu;

    # fix spaces
    $line =~ s/^\s*,|\s*,\s*$//gsi;
    $line =~ s/,\s*,/,/gsi;
    $line =~ s/\n+//gsi;

    print $line, "\n";

}

exit 0;
