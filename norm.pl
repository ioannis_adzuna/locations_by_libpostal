#/usr/bin/env perl

use strict;
use warnings;
use utf8;

use Encode qw(decode);
use I18N::Langinfo qw(langinfo CODESET);

use Container;
use Context;
use Functions::Normalizer;
use Domain::Location;

# set binmode
my $codeset = langinfo(CODESET);
binmode STDOUT, ":encoding($codeset)";


my $context = shift || 'jobs_RU';
my $attribute = shift || 'location';

Context->current->set_context($context);

my $normalizer = Functions::Normalizer->new();

my $attribute_method = "normalize_$attribute";

while ( my $line = readline(STDIN) )
{
    $line = decode($codeset, $line);
    chomp($line);

    my $norm_result = $normalizer->$attribute_method(field => $attribute, values => { $attribute => $line, geo_lat => "", geo_lng => "", geo_inferred => "" } );

    my $location_id = $norm_result->{'location_id'};
    my $location_name = Domain::Location->get($location_id)->label;
    my $location_defaulted = $norm_result->{'location_defaulted'} ? 'DEFAULT' : 'NORMAL';

    my $out_line = sprintf('"%s","%d","%s","%s"', $line, $location_id, $location_name, $location_defaulted);

    print $out_line, "\n";


}

exit 0;
