#!/usr/bin/env perl

use strict;
use warnings;
use utf8;

use Data::Dumper;
use Encode qw(decode);
use I18N::Langinfo qw(langinfo CODESET);
use Text::CSV;

# set binmode
my $codeset = langinfo(CODESET);
binmode STDOUT, ":encoding($codeset)";


my $csv = Text::CSV->new ({ binary => 1, auto_diag => 1 });
while (my $row = $csv->getline(\*STDIN)) {

    my $out_line = join ', ', @$row;
    print $out_line, "\n";

}

exit 0;
