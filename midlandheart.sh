mkdir -p runs/midlandheart;

cd runs/midlandheart;

cat ../../data/midlandheart.csv \
|perl ../../csv2str.pl |tee csv2str.out \
|perl ../../preprocess-fix.pl |tee preprocess-fix.out \
|perl ../../convert.pl |tee convert.out \
|perl ../../norm.pl jobs |tee norm.out \
> midlandheart.out;

cd -;
