mkdir -p runs/yandextalents;

cd runs/yandextalents;

cat ../../data/yandextalents-0.001-locations.csv \
|perl ../../csv2str.pl |tee csv2str.out \
|perl ../../preprocess-fix.pl |tee preprocess-fix.out \
|perl ../../convert.pl |tee convert.out \
|perl ../../norm.pl |tee norm.out \
> yandextalents.out;

cd -;
