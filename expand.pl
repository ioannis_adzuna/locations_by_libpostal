#!/usr/bin/env perl

use strict;
use warnings;
use utf8;

use Data::Dumper;
use Geo::libpostal ':all';
use Encode qw(decode);
use I18N::Langinfo qw(langinfo CODESET);

# set binmode
my $codeset = langinfo(CODESET);
binmode STDOUT, ":encoding($codeset)";


my $COMPONENTS =
    # $ADDRESS_NONE |
    # $ADDRESS_ANY |
    # $ADDRESS_NAME |
    # $ADDRESS_HOUSE_NUMBER |
    # $ADDRESS_STREET |
    # $ADDRESS_UNIT |
    $ADDRESS_LOCALITY |
    $ADDRESS_ADMIN1 |
    $ADDRESS_ADMIN2 |
    $ADDRESS_ADMIN3 |
    $ADDRESS_ADMIN4 |
    $ADDRESS_ADMIN_OTHER |
    # $ADDRESS_COUNTRY |
    $ADDRESS_POSTAL_CODE |
    # $ADDRESS_NEIGHBORHOOD |
    # $ADDRESS_ALL |
    0;

my %EXPANSION_OPTS = (
    ###
    languages => [qw( ru )],
    components => $COMPONENTS,
    ###
    latin_ascii => 1,
    # decompose => 1,
    transliterate => 1,
    # strip_accents => 1,
    # lowercase => 1,
    # trim_string => 1,
    # drop_parentheticals => 1,
    # replace_numeric_hyphens => 1,
    # delete_numeric_hyphens => 1,
    # split_alpha_from_numeric => 1,
    # replace_word_hyphens => 1,
    # delete_word_hyphens => 1,
    # delete_final_periods => 1,
    # delete_acronym_periods => 1,
    # drop_english_possessives => 1,
    # delete_apostrophes => 1,
    # expand_numex => 1,
    # roman_numerals => 1,
);


while  ( my $line = readline(STDIN) )
{
    $line = decode($codeset, $line);

    my $out_line = Geo::libpostal::expand_address($line, %EXPANSION_OPTS);

    print $out_line, "\n";
}

exit 0;
